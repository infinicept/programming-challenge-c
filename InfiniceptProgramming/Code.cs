﻿using System;
using System.Collections.Generic;

namespace InfiniceptProgramming
{
    /// <summary>
    /// Implement these methods!
    /// Instructions are in the Tests file.
    /// </summary>
    public class Code
    {
        public static bool IsPalindrome(string input)
        {
            throw new NotImplementedException();
        }

        public static int GetIndexOfFirstCharacterOfSubstring(string inputString, string subString)
        {
            throw new NotImplementedException();
        }

        public static int[] ReverseArray(int[] input)
        {
            throw new NotImplementedException();
        }

        public static int[] FindDuplicates(int[] input)
        {
            throw new NotImplementedException();
        }

        public static Coins MakeChange(decimal money)
        {
            throw new NotImplementedException();
        }
        
        public static bool CanViewAllMovies(Movie[] movies)
        {
            throw new NotImplementedException();
        }

        /// <hint>
        /// You may want to look into using some C# XML parsing libraries for this one. That would be encouraged.
        /// </hint>
        public static IEnumerable<int> GetLogEntryIdsByMessage(string xml, string message)
        {
            throw new NotImplementedException();
        }

        public static int BinarySearchTreeNodeDistance(TreeNode tree, int node1, int node2)
        {
            throw new NotImplementedException();
        }

        public static TreeNode MakeBinarySearchTree(List<int> values)
        {
            throw new NotImplementedException();
        }

        public static TreeNode SearchTree(TreeNode tree, int nodeValue)
        {
            throw new NotImplementedException();
        }

        public static int TotalGameScore(string[] scores, int numScores)
        {
            throw new NotImplementedException();
        }
    }
}
